﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class sonar : MonoBehaviour
{
    public AudioSource sonarNote;

    public float distance = 1.0f;

    [SerializeField]  public RawImage depthMap;
    Texture2D depthTexture;
    Color centerColor;
    public Image centerPixelOutpout;




    public void sendSonarWave() {
        //at the end
        sonarNote.Play();
        
        //detect value of pixel
        depthTexture = TextureToTexture2D(depthMap.texture);
        centerColor = depthTexture.GetPixel(depthTexture.width, depthTexture.height);
        distance = centerColor.grayscale;
        centerPixelOutpout.color = centerColor;

        distance = utils.Remap(distance, 0, 1, 3, 0);

        Debug.Log("greyscale is: " + distance);

        StartCoroutine(Countdown(distance));
    }

    
    private IEnumerator Countdown(float duration)
    {
        

        float normalizedTime = 0;
        while (normalizedTime <= 1f)
        {
           
            normalizedTime += Time.deltaTime / duration;
         
            yield return null;
           
        }
        sendSonarWave();
        
    }


 

    private Texture2D TextureToTexture2D(Texture texture)
    {
        Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
        RenderTexture currentRT = RenderTexture.active;
        RenderTexture renderTexture = RenderTexture.GetTemporary(texture.width, texture.height, 32);
        Graphics.Blit(texture, renderTexture);

        RenderTexture.active = renderTexture;
        texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        texture2D.Apply();

        RenderTexture.active = currentRT;
        RenderTexture.ReleaseTemporary(renderTexture);
        return texture2D;
    }

  
}
