﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class arxDepthMap : MonoBehaviour
{
    Texture2D arxCamInput;
    Texture2D depthMap;
    Texture2D textureL;
    Texture2D textureR;
    Vector2 arxResolution = new Vector2(1280, 480);
    public RawImage leftImage;
    public RawImage rightImage;
    public RawImage sourceImage;



    Texture2D calculateDepthMap() {

        extractStereoTextures();

        return depthMap;

    }

    private void Update()
    {
        extractStereoTextures();
        
    }


    void extractStereoTextures() {

        arxCamInput = (Texture2D) sourceImage.texture;

        Color[] c = arxCamInput.GetPixels(0, 0, (int)arxResolution.x/2, (int)arxResolution.y);
        textureL = new Texture2D((int)arxResolution.x / 2, (int)arxResolution.y);
        textureL.SetPixels(c);
        textureL.Apply();
        leftImage.texture = textureL;

        c = arxCamInput.GetPixels((int)arxResolution.x / 2, 0, (int)arxResolution.x / 2, (int)arxResolution.y);
        textureR = new Texture2D((int)arxResolution.x / 2, (int)arxResolution.y);
        textureR.SetPixels(c);
        textureR.Apply();
        rightImage.texture = textureR;

    }

}
