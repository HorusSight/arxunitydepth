﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class discoverDevices : MonoBehaviour
{


    public AudioSource micAudioSource;
    public AudioSource speakerAudioSource;
    int audioInputIndex;
    int audioOutputIndex;
    int camInputIndex;

    bool camAttached = false;
    bool micAttached = false;
    bool speakerAttached = false;
    bool simStarted = false;

    public GameObject startButton;

    WebCamTexture webcamTexture;
    public RawImage cameraDisplay;

    private int minFreq;
    private int maxFreq;
    int recordingTime= 3000;

    public RawImage camfeedback;
    public RawImage micFeedback;
    public Sprite circlefullAsset;
    public Sprite circleEmptyAsset;

    public Button startBtn;


    // Start is called before the first frame update
    void Start()
    {
        webcamTexture = new WebCamTexture();
        startButton.SetActive(false);


    }

    // Update is called once per frame
    void Update()
    {
        if (camAttached && micAttached && !simStarted) {
            startButton.SetActive(true);
        }

    }

    //check devices
    //when 3 devices found display start
    //when start sim start recoridng audio/video and playing audio in the headset

    public void refresh() {
        listDevices();
    }

    public void stopSim()
    {

        micFeedback.texture = circleEmptyAsset.texture;
        camfeedback.texture = circleEmptyAsset.texture;
        Microphone.End(Microphone.devices[audioInputIndex]);
        speakerAudioSource.Stop();
        webcamTexture.Stop();



    }

    public void startSim() {

        if (!simStarted)
        {
            InvokeRepeating("startMicRecording", recordingTime, 1);
            startMicRecording();
            //micAudioSource.clip = Microphone.Start(Microphone.devices[audioInputIndex], true, 10, maxFreq);
            //micAudioSource.Play();
            //micAudioSource.GetComponent<audioAnalysis>().ready = true;
            speakerAudioSource.Play();

            webcamTexture.Play();
            cameraDisplay.texture = webcamTexture;

            simStarted = true;

            startBtn.GetComponentInChildren<Text>().text = "STOP";
        }

        else {

            simStarted = false;
            stopSim();
            startBtn.GetComponentInChildren<Text>().text = "START";

        }
    }

    void startMicRecording(){

        micAudioSource.clip = Microphone.Start(Microphone.devices[audioInputIndex], true, recordingTime, maxFreq);
        micAudioSource.Play();
        micAudioSource.GetComponent<audioAnalysis>().ready = true;

      


    }

    public void listDevices() {
        listCameraDevice();
        listAudioInputDevice();
        //listAudioOutputDevice();

    }

    void listAudioOutputDevice() {

        //string[] devices = Spea.devices;

        //for (int i = 0; i < devices.Length; i++)
        //{
        //    Debug.Log(devices[i]);

        //    string deviceName = devices[i].ToString();

        //    if (deviceName.Contains("CODEC"))
        //    {


        //        Debug.Log("Arx MIC found");

        //        micAttached = true;
        //        audioInputIndex = i;
        //    }
        //}
    }

    void listAudioInputDevice() {



        string[] devices = Microphone.devices;

        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i]);

            string deviceName = devices[i].ToString();

            if (deviceName.Contains("CODEC"))
            {



                micAttached = true;
                audioInputIndex = i;
                Microphone.GetDeviceCaps(deviceName, out minFreq, out maxFreq);

                Debug.Log("Arx Audio found min: "+minFreq+" and max: "+ maxFreq);

                micFeedback.texture = circlefullAsset.texture;


            }
        }



    }



void listCameraDevice()
    {

        WebCamDevice[] devices = WebCamTexture.devices;

        for (int i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);

            string deviceName = devices[i].name;

            if (deviceName.Contains("StereoCam"))
            {
                

                Debug.Log("Arx CAM found");

                camAttached = true;
                camInputIndex = i;
                webcamTexture.deviceName = devices[i].name;

                camfeedback.texture = circlefullAsset.texture;

            }
        }
    }


}
